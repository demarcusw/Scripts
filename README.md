# 0x0 Description

This repo serves to hold scripts that I develop to complete meaningless/meaningful tasks that I don't want to do by hand. It will range from things like simply updating a machine to doing recon or maintaining access to a target (attack/defense competitions).

## 0x1 Sections
* `my_scripts` holds my own owrk
* `3rd_party` will hold the open-source work of others that I have accumulated and regularly use