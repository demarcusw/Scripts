# 0x0 Description
Quick and dirty multi-threaded port scanner written in python. Meant to be used if you land on a box and don't have NMAP installed, or you just don't want to use it.

## 0x1 Usage
* Requires Python3
* ./scanner.py
