#!/usr/bin/env python3
"""
Quick and dirty multi-threaded PortScanner for when you land on a box and you don't have nmap, or dont want to use it
Author: DeMarcus Williams
"""

import socket
import threading
import time
from datetime import datetime
from threading import Thread

MIN_PORT = 1
MAX_PORT = 65535
MAX_THREADS = 128
BAD_URI_1 = 'http://'
BAD_URI_2 = 'https://'

SCAN_RESULTS = []


class Scanner(Thread):
    def __init__(self):
        Thread.__init__(self)
        self.host = ''
        self.port = 0

    @staticmethod
    def tcp_connect(host, port, delay):
        tcp_sock = socket.socket()
        tcp_sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        tcp_sock.settimeout(delay)

        try:
            code = tcp_sock.connect_ex((host, port))
            if code == 0:
                temp = "[+] Port {}: Open!\n".format(port)
                SCAN_RESULTS.append(temp)
        except socket.error as e:
            temp = "Something went wrong!\n {}".format(str(e))
            SCAN_RESULTS.append(temp)

    @staticmethod
    def scan_host(host, delay):
        threads = []

        for i in range(MIN_PORT, MAX_PORT):
            t = threading.Thread(target=Scanner.tcp_connect, args=(host, i, delay))
            if len(threads) < MAX_THREADS:
                threads.append(t)

        for i in range(len(threads)):
            threads[i].start()

        for i in range(len(threads)):
            if threads[i].isAlive():
                threads[i].join()

    @staticmethod
    def go(*args):
        try:
            host = args[0]
            delay = int(args[1])

            host_ip = socket.gethostbyname(host)

            SCAN_RESULTS.append("[+] Host: {} IP: {}\n".format(host, host_ip))
            SCAN_RESULTS.append("[+] Scan started at {} \n".format(time.strftime("%H:%M:%S")))

            start_time = datetime.now()

            # Start scan
            Scanner.scan_host(host_ip, delay)

            stop_time = datetime.now()
            total_time = stop_time - start_time

            SCAN_RESULTS.append("[+] Scan finished at {} \n".format(time.strftime("%H:%M:%S")))
            SCAN_RESULTS.append("[+] Scan duration {} ".format(total_time))
        except IndexError:
            SCAN_RESULTS.append("Incorrect amount of arguments! Check the help menu...\n")

    @staticmethod
    def results():
        return SCAN_RESULTS


def sanitize_host(h):
    if BAD_URI_1 in h:
        h.replace(BAD_URI_1, '')
        return h
    if BAD_URI_2 in h:
        h.replace(BAD_URI_2, '')
        return h
    return h


def main():
    host = input('[*] Host: ')
    host = sanitize_host(host)
    delay = int(input('[*] Delay (in seconds): '))
    s = Scanner()

    s.scan_host(host, delay)
    for e in SCAN_RESULTS:
        print(e)


if __name__ == '__main__':
    main()
