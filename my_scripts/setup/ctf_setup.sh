# get up to date
echo "[+] UPGRADING SYSTEM"
apt update
apt upgrade -y

# install necessary packages for most stuff
echo "[+] Installing shit you'll probably need"
apt install -y git vim wget curl gdb fish python3-pip python3-dev libssl-dev libffi-dev build-essential gcc-multilib clang

# vim config
echo "[+] Setting up VIM"
git clone --depth=1 https://github.com/amix/vimrc.git ~/.vim_runtime
sh ~/.vim_runtime/install_awesome_vimrc.sh

# Pwntools
echo "[+] Setting up PWNtools"
pip install --upgrade --user pwntools

# GDB set
echo "[+] Setting up GDB"
wget -q -O- https://github.com/hugsy/gef/raw/master/scripts/gef.sh | sh
